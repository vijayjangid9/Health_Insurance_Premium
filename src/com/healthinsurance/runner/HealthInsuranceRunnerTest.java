package com.healthinsurance.runner;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.healthinsurance.premium.HealthInsurancePremium;

class HealthInsuranceRunnerTest {

	@Test
	void testMale() {
		int expected = 6237;
		HealthInsurancePremium hpi = new HealthInsurancePremium("abc", "male", 45, false, false, true, true, true,
				false, true, false);

		int actual = hpi.calculatePremium();
		assertEquals(expected, actual);
	}

	@Test
	void testFemale() {
		int expected = 6115;
		HealthInsurancePremium hpi = new HealthInsurancePremium("abc", "female", 30, false, false, true, true, true,
				false, true, false);

		int actual = hpi.calculatePremium();
		assertEquals(expected, actual);
	}

	@Test
	void testOther() {
		int expected = 6115;
		HealthInsurancePremium hpi = new HealthInsurancePremium("abc", "other", 30, false, false, true, true, true,
				false, true, false);

		int actual = hpi.calculatePremium();
		assertEquals(expected, actual);
	}
}
