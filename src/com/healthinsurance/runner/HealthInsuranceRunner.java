package com.healthinsurance.runner;

import java.util.Scanner;

import com.healthinsurance.premium.HealthInsurancePremium;

public class HealthInsuranceRunner {
	public static void main(String[] args) throws Exception {
		System.out.println("enter the details: ");
		Scanner sc = new Scanner(System.in);
		System.out.print("Name: ");
		String name = sc.nextLine();
		System.out.println("Gender(Male/Female/Other): ");
		String gender = sc.nextLine();
		System.out.println("Age: ");
		int age = Integer.parseInt(sc.nextLine());

		System.out.println("Current health:");

		System.out.println("Hypertension(Yes/No): ");
		boolean hypertention = convertToBoolean(sc.nextLine());
		System.out.println("Blood pressure(Yes/No): ");
		boolean bp = convertToBoolean(sc.nextLine());
		System.out.println("Blood sugar(Yes/No): ");
		boolean bs = convertToBoolean(sc.nextLine());
		System.out.println("Overweight(Yes/No): ");
		boolean overwieght = convertToBoolean(sc.nextLine());

		System.out.println("Habits: ");

		System.out.println("Smoking(Yes/No): ");
		boolean smoke = convertToBoolean(sc.nextLine());
		System.out.println("Alcohol(Yes/No): ");
		boolean alcohol = convertToBoolean(sc.nextLine());
		System.out.println("Daily exercise(Yes/No): ");
		boolean exercise = convertToBoolean(sc.nextLine());
		System.out.println("Drugs(Yes/No): ");
		boolean drug = convertToBoolean(sc.nextLine());
		
		sc.close();

		HealthInsurancePremium hip = new HealthInsurancePremium(name, gender, age, hypertention, bp, bs, overwieght,
				alcohol, drug, exercise, smoke);

		System.out.println("Health Insurance Premium for " + name + " is: Rs." +hip.calculatePremium());
	}

	private static boolean convertToBoolean(String str) throws Exception {
		if (str != null) {
			if (str.equalsIgnoreCase("yes"))
				return true;
			else if (str.equalsIgnoreCase("no"))
				return false;
			else
				throw new Exception("Invalid argument");
		} else {
			throw new Exception("Input is null");
		}
	}
}
