package com.healthinsurance.premium;

import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class HealthInsurancePremium {

	final static double BASE_PREMIUM = 5000D;

	String name;
	String gender;
	int age;
	boolean hypertention;
	boolean bp;
	boolean bs;
	boolean overwieght;

	boolean alcohol;
	boolean drug;
	boolean exercise;
	boolean smoke;

	public HealthInsurancePremium(String name, String gender, int age, boolean hypertention, boolean bp, boolean bs,
			boolean overwieght, boolean alcohol, boolean drug, boolean exercise, boolean smoke) {
		this.name = name;
		this.gender = gender;
		this.age = age;
		this.hypertention = hypertention;
		this.bp = bp;
		this.bs = bs;
		this.overwieght = overwieght;
		this.alcohol = alcohol;
		this.drug = drug;
		this.exercise = exercise;
		this.smoke = smoke;
	}

	private double calculatePremiumForAge() {
		double premium = BASE_PREMIUM;

		if (age < 18) {
			return premium;
		} else if (age >= 18 && age <= 25)
			premium = premium + ((10 * premium) / 100);
		else if (age >= 26 && age <= 30)
			premium = premium + ((20 * premium) / 100);
		else if (age >= 31 && age <= 35)
			premium = premium + ((30 * premium) / 100);
		else if (age >= 36 && age <= 40)
			premium = premium + ((40 * premium) / 100);
		else if (age > 40) {
			int temp = age - 40;
			double ext = temp / 5;
			double rmd = temp % 5;
			if (rmd > 0)
				ext++;

			premium = premium + ((20 * ext * premium) / 100);
		}

		return premium;
	}

	private double calculatePremiumForGender(double stdPremium) {
		double premium = stdPremium;

		if (gender.equalsIgnoreCase("Male"))
			premium = premium + ((2 * premium) / 100);

		return premium;
	}

	private double calculatePremiumForHealthConditions(double stdPremium) {
		double premium = stdPremium;

		if (hypertention)
			premium = premium + ((1 * premium) / 100);
		if (bp)
			premium = premium + ((1 * premium) / 100);
		if (bs)
			premium = premium + ((1 * premium) / 100);
		if (overwieght)
			premium = premium + ((1 * premium) / 100);

		return premium;
	}

	private double calculatePremiumForBadHabits(double stdPremium) {
		double premium = stdPremium;

		if (smoke)
			premium = premium + ((3 * premium) / 100);
		if (alcohol)
			premium = premium + ((3 * premium) / 100);
		if (drug)
			premium = premium + ((3 * premium) / 100);

		return premium;
	}

	private double calculatePremiumForGoodHabits(double stdPremium) {
		double premium = stdPremium;

		if (exercise)
			premium = premium - ((3 * premium) / 100);

		return premium;
	}

	public int calculatePremium() {
		double premium = calculatePremiumForAge();
		premium = calculatePremiumForGender(premium);
		premium = calculatePremiumForHealthConditions(premium);
		premium = calculatePremiumForBadHabits(premium);
		premium = calculatePremiumForGoodHabits(premium);

		return (int) premium;
	}
}
